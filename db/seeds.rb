200000.times do |i|
  post_params = {title: "title#{i}", body: "body text #{i}", author_ip: "123.123.123.#{rand(1..50)}", author_login: "author_#{rand(1..100)}"}
  post = Form::Post.new(post_params)
  post.author = Form::Author.find_or_create_by(login: post_params[:author_login])
  post.save
end

5000.times do
  rate_params = {post_id: rand(50..777), value: rand(1..5)}
  Form::Rate.create(rate_params)
end
