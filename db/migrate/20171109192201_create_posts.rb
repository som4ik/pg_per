class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.string :title
      t.text :body
      t.string :author_ip
      t.bigint :author_id, foreign_key: true, index: true

      t.timestamps
    end
  end
end
