Rails.application.routes.draw do
  namespace "v1" do
    resources :posts, only: [:create, :index] do
      resources :rates, only: :create, controller: "posts_rates"
    end

    resources :services, only: [] do
      collection do
        get :connections
      end
    end
  end
end
