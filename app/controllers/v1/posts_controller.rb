class V1::PostsController < ApplicationController

  #
  # return top n posts with best rate
  # => notice: if no params given it return top 10 to escape 500
  #
  # @return [Json]
  #
  def index
    limit = params[:limit]
    posts = Post.top(limit)
    render json: posts
  end

  #
  # responsible of create posts by requests
  #
  # @return [Json]
  #
  def create
    post = Form::Post.new(post_params)

    post.author = find_or_create_author

    if post.valid?
      post.save
      render json: post, status: :created
    else
      render json: {error: post.errors}, status: :unprocessable_entity
    end

  end

  private

  #
  # create of assign author only for valid post
  #
  # @return [Object-Form::Author / nil]
  #
  def find_or_create_author
    Form::Author.find_or_create_by(login: post_params[:author_login])
  end

  def post_params
    params.permit(:title, :body, :author_ip, :author_login)
  end
end
