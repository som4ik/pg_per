class V1::PostsRatesController < ApplicationController

  #
  # create
  #
  # @return [<type>] <description>
  #
  def create
    post = Post.find(rate_params[:post_id])
    rate = Form::Rate.new(rate_params)

    if rate.save
      render json: { new_avarage_rate: post.average_rate }
    else
      render json: {errors: rate.errors}, status: :unprocessable_entity
    end
  end



  private

  def rate_params
    params.permit(:post_id, :value)
  end

end
