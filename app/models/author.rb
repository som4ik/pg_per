class Author < ApplicationRecord
  self.table_name = "users"
  has_many :posts
end
