#
# Class Form::Post provides validation with title, body, author_ip, author_id
#
# @author Somar Melhem < somar_melhem@hotmail.com >
#
class Form::Post < Post
  attr_accessor :author_login

  def valid?(context= nil)
    self.errors.clear
      with_title?
      with_body?
      with_author_ip?
      with_author_login?
      with_author?
    self.errors.blank?
  end

  private

  %w( title body author_ip author_login).each do |to_validate|
    define_method( "with_#{to_validate}?".to_sym ) do
      errors.add(to_validate.to_sym, :blank) if self.send(to_validate.to_sym).nil?
      is_valid_ip? if to_validate.to_sym == :author_ip && self.author_ip
    end
  end

  def with_author?(author: Form::Author)
    if self.errors.blank? && !self.author.present?
      errors.add(:author, :blank)
    end
  end

  def is_valid_ip?
    case self.author_ip
    when Resolv::IPv4::Regex || Resolv::IPv6::Regex
      return
    else
      errors.add(:author_ip, message: "should be valid IP address")
    end
  end
end
