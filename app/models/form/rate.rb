class Form::Rate < Rate

  VALUE_VALID_RANG = [1,2,3,4,5]

  def valid?(context=nil)
    self.errors.clear
    with_value?
    with_post?
    self.errors.blank?
  end

  private

  def with_value?
    if self.value.nil?
      errors.add(:value, :blank)
    elsif !self.value.in?(Form::Rate::VALUE_VALID_RANG)
      errors.add(:value, message: "Not valid value, should be Integer between 1-5.")
    end
  end

  def with_post?
    errors.add(:post, :blank) if self.post.nil?
  end
end
