class Form::Author < Author

  #
  # override rails valid method
  #
  # @param [Symbole] context default context of super method ex: [:create , :update, etc..]
  #
  # @return [Boolean]
  #
  def valid?(context = nil)
    self.errors.clear

    with_login?
    with_uniq_login?
    self.errors.blank?
  end

  private

  def with_uniq_login?
    if Author.where(login: self.login).present?
      errors.add(:login, message: "duplicated")
    end
  end

  def with_login?
    errors.add(:login, :blank) if login.nil?
  end
end
