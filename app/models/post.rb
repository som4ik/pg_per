class Post < ApplicationRecord
  belongs_to :author, class_name: "Author", foreign_key: :author_id
  has_many :rates, dependent: :destroy

  # give limit default value to escape 500
  scope :top, ->(n = 10){
    joins(:rates) \
    .group("posts.id")
    .order("AVG(rates.value) DESC")
    .select(:title, :body)
    .limit(n)
  }


  def average_rate(rate: Rate )
    rate.where(post_id: self.id).average(:value).to_i
  end

  #
  # listable ip address
  #
  # @return [Json objects]
  #
  def self.listabel_ip_addresses
    connection_pears = {}
    Post.joins(:author).select("posts.id, posts.author_ip, users.login as login").find_each(batch_size: 1000) do |post_with_login|
      connection_pears[post_with_login.author_ip] ||= Set.new
      connection_pears[post_with_login.author_ip] << post_with_login.login
    end
    [connection_pears]
  end
end
