require "rails_helper"

RSpec.describe Form::Rate do
  context "validation attributes" do

    let(:post) {
        Post.create(author: Author.create)
    }

    let(:rate) {
      Form::Rate.new(
        post_id: post.id,
        value: 4
      )
    }

    it "shouldn't be valid without post_id" do
      rate.post_id = nil
      expect(rate.valid?).to eq(false)
    end

    it "shouldn't be valid without value" do
      rate.value = nil
      expect(rate.valid?).to eq(false)
    end

    it "shouldn't be valid with value not from 1 to 5" do
      rate.value = 6
      expect(rate.valid?).to eq(false)

      rate.value = 0
      expect(rate.valid?).to eq(false)

      rate.value = 3
      expect(rate.valid?).to eq(true)
    end
  end
end
