require "rails_helper"

RSpec.describe Form::Post do

  describe "Form Post validations" do

    let!(:author) { Form::Author.create(login: "what ever login")}

    let!(:post) {  Form::Post.new(
                    title: "what ever",
                    body: "what ever",
                    author_ip: "123.123.123.12",
                    author_login: "author_login"
                  )
                }
    before :each do
      post.author = author
      expect(post.valid?).to eq(true)
    end

    it "shouldn't be valid with out title" do
      post.title = nil
      expect(post.valid?).to eq(false)
    end

    it "shouldn't be valid with out body" do
      post.body = nil
      expect(post.valid?).to eq(false)
    end

    it "shouldn't be valid with out author_ip" do
      post.author_ip = nil
      expect(post.valid?).to eq(false)
    end

    it "shouldn't be valid without author_login" do
      post.author_login = nil
      expect(post.valid?).to eq(false)
    end

    it "shouldn't be valid with not valid IP" do
      post.author_ip = "123.12"
      expect(post.valid?).to eq(false)
    end
  end
end
