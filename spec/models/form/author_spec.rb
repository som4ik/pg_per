require "rails_helper"

RSpec.describe Form::Author do
  describe "login validation" do

    let!(:author) { Form::Author.new }

    it "Not valid without login" do
      expect(author.valid?).to eq(false)
    end

    it "valid with login" do
      author.login = "what_ever"
      expect(author.valid?).to eq(true)
    end
  end
end
