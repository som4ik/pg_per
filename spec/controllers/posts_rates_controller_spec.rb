require "rails_helper"

RSpec.describe V1::PostsRatesController, as: :controller do
  describe "#Create rate for posts" do
    it "what ever " do
      #  create a dummy post
      author = Author.create(login: "test")
      post_1 = Post.create(author_id: author.id)

      post :create, params: {post_id: post_1.id, value: 1}
      expect(response.code).to eq("200")
      expect(Rate.last.value).to eq(1)
    end

    it "give avarage of post rates" do
      author = Author.create(login: "test")
      post_1 = Post.create(author_id: author.id)
      post :create, params: {post_id: post_1.id, value: 2}
      post :create, params: {post_id: post_1.id, value: 4}

      reponse_body = (JSON.parse(response.body))
      expect(reponse_body["new_avarage_rate"]).to eq(3)
    end
  end
end
