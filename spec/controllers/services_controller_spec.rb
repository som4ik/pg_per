require "rails_helper"

RSpec.describe V1::ServicesController, as: :controller do

  describe "get pears of author_ip and its logins" do
    it "fetch response with status 200" do
      ## prepare data
      15.times do |i|
        author = Form::Author.create(login: "login#{i}")
        post = Form::Post.new(title: "title#{i}",
                    body: "body:#{i}",
                    author_ip: "123.123.123.12#{i}",
                    author_login: "login#{i}")
        post.author = author
        post.save
      end

      get :connections
      expect(response.status).to eq(200)
    end
  end
end
