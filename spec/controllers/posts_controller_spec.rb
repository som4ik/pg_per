require "rails_helper"

RSpec.describe V1::PostsController, as: :controller do
  before :each do
    Post.all.destroy_all
    Author.all.destroy_all
  end
  describe "creation post v1/posts#create" do
    it "#create should create post and author" do
      params =  {
                  title: "test_title",
                  body: "test_body",
                  author_ip: "123.123.123.123",
                  author_login: "test_login"
                }

      post :create, params: params
      expect(response.status).to eq(201)
      expect(Post.count).to eq(1)
      expect(Author.count).to eq(1)
    end

    it "#create should create post for existing author" do
      Form::Author.create(login: "test_author")
      params =  {
                  title: "test_title",
                  body: "test_body",
                  author_ip: "123.123.123.123",
                  author_login: "test_author"
                }
      post :create, params: params
      expect(response.status).to eq(201)
      expect(Post.count).to eq(1)
    end

    it "should return status 422 Unprocessable Entity with empty params" do
      post :create
      expect(response.code).to eq("422")
    end

    it "shouldn't create post/author nad return status 422 if moderate params are missing" do
      post :create, params: {title: "test"}

      expect(response.status).to eq(422)
    end
  end

  describe "get posts v1/posts#index" do
    it "#index return posts" do
      ## prepare data
      15.times do |i|
        author = Form::Author.create(login: "login#{i}")
        post = Form::Post.new(title: "title#{i}",
                    body: "body:#{i}",
                    author_ip: "123.123.123.12#{i}",
                    author_login: "login#{i}")
        post.author = author
        post.save
      end
      Form::Post.all.each do |post|
        5.times do |i|
          post.rates.create(value: i)
        end
      end

      ## end of prepare data # no time to factoryGirl and support
      get :index
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body).count).to be >= 10

      get :index, params: {limit: 5}
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body).count).to be(5)
    end
  end
end
